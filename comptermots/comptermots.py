# -*- coding: utf-8 -*-
class CompterMots:
    def count(self, phrase):
        asciiSpace = {"\t": 1, "\n": 1, "\b": 1, "\r": 1, " ": 1}
        nb = 0
        i = 0
        motStart = 0
        while i < len(phrase) and phrase[i] in asciiSpace:
            i = i + 1
        motStart = i
        while i < len(phrase):
            if not (phrase[i] in asciiSpace):
                i = i + 1
                continue
            nb = nb + 1
            i = i + 1
            while i < len(phrase) and phrase[i] in asciiSpace:
                i = i + 1
            motStart = i
        if motStart < len(phrase):
            nb = nb + 1
        return nb
