#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_comptermots
----------------------------------

Tests for `comptermots` module.
"""

from comptermots.comptermots import CompterMots


def test_compteVide():
    c = CompterMots()
    res = c.count("")
    assert res == 0


def test_compteUnMot():
    c = CompterMots()
    res = c.count("test")
    assert res == 1


def test_compte4Mot():
    c = CompterMots()
    res = c.count("ceci est un test")
    assert res == 4


def test_compte4MotAvecUnBlancAvAp():
    c = CompterMots()
    res = c.count("      ceci est un test   ")
    assert res == 4


def test_compte4MotAvecRcEtUnBlancAvAp():
    c = CompterMots()
    res = c.count("      ceci\nest un\ttest \n   ")
    assert res == 4
